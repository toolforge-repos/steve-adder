// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright 2022 Kunal Mehta <legoktm@debian.org>
#[macro_use]
extern crate rocket;

use anyhow::Result;
use csv::ReaderBuilder;
use indexmap::IndexMap;
use rocket::http::Status;
use rocket_dyn_templates::Template;
use rocket_healthz::Healthz;
use serde::{de, Deserialize, Serialize};
use std::collections::HashSet;

type Data = IndexMap<String, Vec<Row>>;

#[derive(Deserialize, Debug)]
struct Row {
    date: String,
    browser_family: String,
    #[serde(deserialize_with = "deserialize_dash_number")]
    browser_major: Option<u64>,
    percent: f64,
}

#[derive(Debug, Deserialize)]
#[serde(untagged)]
enum DashNumber {
    Number(u64),
    // We don't read the value since we just want serde to change based on the type
    #[allow(dead_code)]
    Dash(String),
}

/// Bit of magic to deserialize a "-" as None and a number as Some(u64)
fn deserialize_dash_number<'de, D>(deserializer: D) -> Result<Option<u64>, D::Error>
where
    D: de::Deserializer<'de>,
{
    let val = DashNumber::deserialize(deserializer)?;
    Ok(match val {
        DashNumber::Number(num) => Some(num),
        DashNumber::Dash(_) => None,
    })
}

/// Read all the data and only the most recent set of data. Unfortunately we can't
/// read the CSV backwards to make it faster.
fn read_data() -> Result<Data> {
    // Yes, blocking reads are bad. Shhhh
    let mut reader = ReaderBuilder::new()
        .delimiter(b'\t')
        .from_path("all_sites_by_browser_family_and_major_percent.tsv")?;
    let mut all_rows: Data = IndexMap::new();
    for result in reader.deserialize() {
        let row: Row = result?;
        all_rows.entry(row.date.to_string()).or_default().push(row);
    }
    // Now we just want the last 4 dates
    let mut rows: Data = IndexMap::new();
    for _ in 1..=4 {
        let (key, value) = all_rows.pop().unwrap();
        rows.insert(key, value);
    }
    Ok(rows)
}

/// Return a sorted list of browsers in the dataset
fn browsers(data: &Data) -> Vec<String> {
    let mut ret: HashSet<String> = HashSet::new();
    for rows in data.values() {
        for row in rows {
            ret.insert(row.browser_family.to_string());
        }
    }
    let mut ret: Vec<_> = ret.into_iter().collect();
    ret.sort();
    ret
}

/// Actually do the adding!!!
fn adder(wanted: &[String], start: u64, end: u64, data: &Data) -> f64 {
    let mut val: f64 = 0.0;
    for rows in data.values() {
        for row in rows {
            // If the row matches that browser
            if wanted.contains(&row.browser_family) {
                // And we have a version for it
                if let Some(version) = row.browser_major {
                    // And the version is in [start, end]
                    if version >= start && version <= end {
                        val += row.percent;
                    }
                }
            }
        }
    }
    // Multiply by 100 to be a percent, divide by 4 to take the average of the four weeks
    val * 100.0 / 4.0
}

/// index.html
#[derive(Serialize)]
struct IndexTemplate {
    browsers: Vec<String>,
    date: String,
    wanted: Vec<String>,
    start: Option<u64>,
    end: Option<u64>,
    answer: Option<String>,
}

/// error.html
#[derive(Serialize)]
struct ErrorTemplate {
    error: String,
}

#[get("/?<wanted>&<start>&<end>")]
fn index(
    wanted: Option<Vec<String>>,
    start: Option<u64>,
    end: Option<u64>,
) -> Result<Template, (Status, Template)> {
    let data = match read_data() {
        Ok(data) => data,
        Err(err) => {
            return Err((
                Status::InternalServerError,
                Template::render(
                    "error",
                    ErrorTemplate {
                        error: err.to_string(),
                    },
                ),
            ))
        }
    };
    let browsers = browsers(&data);
    let answer = match (wanted.as_ref(), start, end) {
        // If all three values are present, add up!
        (Some(wanted), Some(start), Some(end)) => {
            let answer = adder(wanted, start, end, &data);
            Some(format!("{:.3}", answer))
        }
        _ => None,
    };
    let dates: Vec<_> = data.keys().map(|val| val.to_string()).collect();
    Ok(Template::render(
        "index",
        IndexTemplate {
            browsers,
            date: dates.join(", "),
            wanted: wanted.unwrap_or_default(),
            start,
            end,
            answer,
        },
    ))
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .mount("/", routes![index])
        .attach(Template::fairing())
        .attach(Healthz::fairing())
}
